<div class="wiki" id="content_view" style="display: block;">

# <a name="Downloads"></a>Downloads

<div id="toc">

# Table of Contents

<div style="margin-left: 1em;">[Downloads](#Downloads)</div>

<div style="margin-left: 2em;">[Description](#Downloads-Description)</div>

<div style="margin-left: 2em;">[Main Program](#Downloads-Main Program)</div>

<div style="margin-left: 2em;">[Additional Files](#Downloads-Additional Files)</div>

<div style="margin-left: 2em;">[Utilities](#Downloads-Utilities)</div>

<div style="margin-left: 2em;">[Images](#Downloads-Images)</div>

</div>

## <a name="Downloads-Description"></a>Description

The following resources are available for free download from, or by re-direction from, this Wiki:  

## <a name="Downloads-Main Program"></a>Main Program

*   [LTVT Program](LTVT%20Download.html) -- _the current version of the LTVT software, including source code_

## <a name="Downloads-Additional Files"></a>Additional Files

*   [Supplemental Files](Supplemental%20Files.html) -- _additional image and dot overlay files, making possible the display of a wide variety of lunar information._
    *   [Dot Files](Dot%20Files.html) -- _supplementary files listing features and data points for display with LTVT_
    *   [Ephemeris Files](Ephemeris%20Files.html) -- _necessary for computing lunar viewing geometry_
    *   [Texture Files](Texture%20Files.html) -- _used for painting the background in LTVT lunar simulations_
        *   [Additional Textures](Additional%20Textures.html)
    *   [DEM Data](Sources%20of%20Planetary%20DEM%20Data.html) -- _sources of digital height information from which images can be simulated_

## <a name="Downloads-Utilities"></a>Utilities

*   [Utility Programs](Utility%20Programs.html) -- _supplementary programs for performing and automating a number of LTVT housekeeping chores, including image downloading._

## <a name="Downloads-Images"></a>Images

*   [Image Resources](Image%20Resources.html) -- _provides links for obtaining lunar images, some pre-calibrated, from the internet for viewing with LTVT_
    *   [Maps](Map%20downloads.html) -- _in projections understood by LTVT_
    *   [Earth-based images](Earth-based%20image%20downloads.html) -- _links to selected amateur images on the web, plus:_
        *   [Consolidated Lunar Atlas](Consolidated%20Lunar%20Atlas.html) -- _calibrated classic image set from 1960's covering the Moon's nearside at moderate resolution_
        *   [Calibrated Full Disk Images](Calibrated%20Full%20Disk%20Images.html)
        *   [Radar Maps](Radar%20Maps.html)
    *   [Satellite images](Satellite%20image%20downloads.html)
        *   [Ranger](Ranger.html) - _early images obtained by spacecraft crashing into lunar surface_
        *   [Lunar Orbiter](Lunar%20Orbiter.html) - _includes calibration data and URL list for automated download of composite images on this website_
            *   [Lunar Orbiter Composites](Lunar%20Orbiter%20Composites.html)
        *   [Apollo](Apollo.html) - _includes calibration data and URL list for samples of Apollo Metric images_
        *   [Clementine](Clementine.html) -- _large data set from 1990's for whole Moon at high sun angle_
        *   [Image Support Data](Image%20Support%20Data.html) -- _resources for determining date, time and location of satellite images not included here_

* * *

This page has been edited 10 times. The last modification was made by <span class="membersnap">- [![JimMosher](files/external-39f6349cba31aa1be9c60f9a09377484.jpg)](http://www.wikispaces.com/user/view/JimMosher) [JimMosher](http://www.wikispaces.com/user/view/JimMosher)</span> on Mar 9, 2010 6:26 pm</div>
